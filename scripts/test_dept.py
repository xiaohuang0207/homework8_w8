from api.dept_api import DeptApi
from common.assert_tools import common_assert
from common.get_req_header import get_req_header


class TestDept(object):
    """借助 pytest 封装测试类"""

    def setup_class(self):
        self.header = get_req_header()

    def test01_add_dept(self):
        data = {"name": "市场部", "code": "250"}
        resp = DeptApi.add_dept(self.header, data)  # 可读性
        print(resp.json())
        common_assert(resp, 200, True, 10000, "操作成功")

    def test02_modify_dept(self):
        dept = DeptApi()
        dept_id = "1651755146939592704"
        req_body = {"name": "干掉市场部", "code": "360"}
        resp = dept.modify_dept(dept_id, self.header, req_body)
        print(resp.json())
        common_assert(resp, 200, True, 10000, "操作成功")

    def test03_query_dept(self):
        pass

    def test04_del_dept(self):
        pass
