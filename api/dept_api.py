import requests

from common.get_req_header import get_req_header


class DeptApi(object):
    """接口对象层"""

    @staticmethod
    def add_dept(req_header, req_body):
        resp = requests.post(url="http://ihrm2-test.itheima.net/api/company/department",
                             headers=req_header, json=req_body)
        return resp

    def modify_dept(self, dept_id, req_header, req_body):
        return requests.put(url="http://ihrm2-test.itheima.net/api/company/department/" + dept_id,
                            headers=req_header, json=req_body)

    def query_dept(self):
        pass

    def del_dept(self):
        pass


if __name__ == '__main__':
    header = get_req_header()
    data = {"name": "市场部", "code": "250"}
    resp = DeptApi.add_dept(header, data)  # 可读性
    print(resp.json())

    dept = DeptApi()
    dept_id = "1651755146939592704"
    req_body = {"name": "干掉市场部", "code": "360"}
    resp = dept.modify_dept(dept_id, header, req_body)
    print(resp.json())
