import requests

from common.get_req_header import get_req_header

dept_id = "1651752981718233088"

resp = requests.post(url="http://ihrm2-test.itheima.net/api/company/department",
                     headers=get_req_header(),
                     json={"name": "市场部", "code": "250"})
print(resp.json())

resp = requests.put(url="http://ihrm2-test.itheima.net/api/company/department/" + dept_id,
                    headers=get_req_header(),
                    json={"name": "干掉市场部", "code": "360"})
print(resp.json())
